import random
import time
import entity_generator
from entity_generator import player_list, country_list
# entity_generator.main(number_of_players=2)

count_year = random.randint(1800, 2000)


def get_stats(player, country):
    print('economy: ', country.industry_list.economy)
    print('food: ', country.industry_list.food)
    print('health: ', country.industry_list.health)
    print('electricity: ', country.industry_list.electricity)
    print('army: ', country.industry_list.military)


def change_stat(country, random_event):
    if random_event.minister.industry == 'electricity':
        if random.randint(0, 1) == 0:
            country.industry_list.electricity = country.industry_list.electricity - random.randint(50, 500)
        else:
            country.industry_list.electricity = country.industry_list.electricity + random.randint(50, 500)
    if random_event.minister.industry == 'economy':
        if random.randint(0, 1) == 0:
            country.industry_list.economy = country.industry_list.economy - random.randint(50, 500)
        else:
            country.industry_list.economy = country.industry_list.economy + random.randint(50, 500)
    if random_event.minister.industry == 'health':
        if random.randint(0, 1) == 0:
            country.industry_list.health = country.industry_list.health - random.randint(50, 500)
        else:
            country.industry_list.health = country.industry_list.health + random.randint(50, 500)
    if random_event.minister.industry == 'military':
        if random.randint(0, 1) == 0:
            country.industry_list.military = country.industry_list.military - random.randint(50, 500)
        else:
            country.industry_list.military = country.industry_list.military + random.randint(50, 500)
    if random_event.minister.industry == 'food':
        if random.randint(0, 1) == 0:
            country.industry_list.food = country.industry_list.food - random.randint(50, 500)
        else:
            country.industry_list.economy = country.industry_list.economy + random.randint(50, 500)
            country.industry_list.food = country.industry_list.food + random.randint(50, 500)


def process_random_event(country):
    # for event in country.event_list:
    option_name_list = []
    option_positive_list = []
    random_event = random.choice(country.event_list)
    for i in random_event.all_options:
        option_name_list.append(i.name)
    # option_positive_list.append(i.positive_text)
    while True:
        print("Player", country.leader, "your move")
        print(random_event.base_text)
        print(random_event.minister.position_name)
        print(random_event.minister.name)
        print("options: ", option_name_list)
        answer = input()
        if answer in option_name_list:
            change_stat(country=country, random_event=random_event)
            get_stats(player=None, country=country)
            break
        else:
            print('sorry say something else')
            print('')
    print('OK')


def run_game():
    global count_year
    entity_generator.main(number_of_players=2)
    country = ''
    while True:
        for country in country_list:
            print("-------------------------------------------------")
            print("year: ", count_year)
            print('')
            process_random_event(country=country)
            print('')
            if country.industry_list.military <= 0:
                print(country.name, 'lose')
                country_list.remove(country)
                if len(country_list) <= 1:
                    print(country.name, 'won')
                break
            elif country.industry_list.health <= 0:
                print(country.name, 'lose')
                country_list.remove(country)
                if len(country_list) <= 1:
                    print(country.name, 'won')
                break
            elif country.industry_list.food <= 0:
                print(country.name, 'lose')
                country_list.remove(country)
                if len(country_list) <= 1:
                    print(country.name, 'won')
                break
            elif country.industry_list.economy <= 0:
                print(country.name, 'lose')
                country_list.remove(country)
                if len(country_list) <= 1:
                    print(country.name, 'won')
                break
            elif country.industry_list.electricity <= 0:
                print(country.name, 'lose')
                country_list.remove(country)
                if len(country_list) <= 1:
                    print(country.name, 'won')
                break
        count_year += 1


run_game()

