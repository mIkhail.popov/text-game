import random_name
import random
import csv

player_list = []
country_list = []
name_list = []
character_traits = ['kind', 'ruthless', 'psychopath', 'fat', 'insubordinate']
full_industry_list = ["economy", "industry", "military", "health", "electricity", "food", "intelligence"]


with open('test.csv ', 'r') as txt:
    sss = csv.reader(txt, delimiter=';')
    for i in sss:
        name_list.append(str(i[0]))

print(name_list)


class Player:
    def __init__(self, name, timeline, dictator_points):
        self.name = name
        self.timeline = timeline
        self.dictator_points = dictator_points


class Country:
    def __init__(self, name, leader, minister_list, industry_list, event_list):
        self.name = name
        self.leader = leader
        self.minister_list = minister_list
        self.industry_list = industry_list
        self.event_list = event_list


class IndustrySet:
    def __init__(self, country, economy, industry, military, health, electricity, food, intelligence, industry_list):
        self.industry_list = industry_list
        self.country = country
        self.economy = economy
        self.industry = industry
        self.military = military
        self.health = health
        self.electricity = electricity
        self.food = food
        self.intelligence = intelligence

    def __iter__(self):
        return iter([attr for attr in dir(IndustrySet) if attr[:2] != "__"])


class Minister:
    def __init__(self, name, position_name, industry, age, character_type, behaviour_type, traits):
        self.name = name
        self.position_name = position_name
        self.industry = industry
        self.age = age
        self.character_type = character_type
        self.behaviour_type = behaviour_type
        self.traits = traits

    def get_traits(self):
        for num in range(random.randint(1, 5)):
            random_trait = random.choice(character_traits)
            if random_trait not in self.traits:
                self.traits.append(random_trait)


class Event:
    def __init__(self, base_text, all_options, consequences, minister, industry):
        self.base_text = base_text
        self.all_options = all_options
        self.consequences = consequences
        self.minister = minister
        self.industry = industry


class Option:
    def __init__(self, name, key_words, linked_industries, min_effect, max_effect, positive_text, negative_text,
                 caused_events):
        self.name = name
        self.key_words = key_words
        self.linked_industries = linked_industries
        self.min_effect = min_effect
        self.max_effect = max_effect
        self.positive_text = positive_text
        self.negative_text = negative_text
        self.caused_events = caused_events


def create_name_of_ministers():
    for minis in range(3):
        first_name = random_name.generate_full_name()[0]
        last_name = random_name.generate_full_name()[1]
        full_name = first_name, last_name
        minister = Minister(name=full_name, position_name=None, industry=None, age=None, character_type=None,
                            behaviour_type=None,
                            traits=None)
    return minister.name


def create_players(number_of_players):
    global player_list
    global country_list
    for n in range(number_of_players):
        print('player ', n + 1, ', enter your name: ')
        player_name = input()
        print(player_name, ', enter the name of your Country: ')
        country_name = input()
        new_player = Player(name=player_name, timeline=[], dictator_points=50)
        player_list.append(new_player)
        new_country = Country(name=country_name, leader=player_name, minister_list=[],
                              industry_list=[], event_list=[])
        new_country.industry_list = IndustrySet(country=new_country, economy=1000, industry=1000, military=1000,
                                                health=1000, electricity=1000, food=1000, intelligence=1000,
                                                industry_list=full_industry_list)
        # print(new_country.industry_list)
        country_list.append(new_country)
    print('LET THE COLD WAR BEGIN!!!')


def generate_ministers():
    global country_list
    for country in country_list:
        industry_list = country.industry_list.industry_list
        for industry in industry_list:
            new_random_name = random_name.generate_full_name()
            new_industry_minister = Minister(name=new_random_name, position_name='Minister of ' + industry,
                                             industry=industry,
                                             age=random.randint(30, 99), character_type='', behaviour_type='',
                                             traits=[])
            new_industry_minister.get_traits()
            country.minister_list.append(new_industry_minister)
            # print(new_industry_minister.name, new_industry_minister.industry, new_industry_minister.traits)


# sub-function of generate_events
def generate_options(id):
    output_list = []
    with open('options_list.csv ', 'r') as txt:
        reader = csv.reader(txt, delimiter=',')
        for row in reader:
            if row[0] == id:
                new_option = Option(name=row[1], key_words=[], linked_industries=[], max_effect=100, min_effect=10,
                                    positive_text=row[2], negative_text=row[3], caused_events=[])
                output_list.append(new_option)
            else:
                pass
        txt.close()
    return output_list


def generate_events(n):
    global country_list
    for country in country_list:
        with open('event_list.csv ', 'r', encoding="utf8") as event_csv:
            reader = csv.reader(event_csv, delimiter=',')
            all_events = list(reader)
            for i in range(n):
                random_event = random.choice(all_events)
                # print(random_event, 'random event')
                # print(reader, 'reader')
                relevant_minister = ''
                for minister in country.minister_list:
                    if minister.industry == random_event[1]:
                        relevant_minister = minister
                    else:
                        pass
                event = Event(minister=relevant_minister, base_text=random_event[0],
                              all_options=generate_options(id=random_event[2]),
                              consequences=[], industry=random_event[1])
                country.event_list.append(event)
    return event
    # txt.close()


def main(number_of_players):
    create_players(2)
    generate_ministers()
    print('generate_ministers')
    generate_events(2)
    print('')

